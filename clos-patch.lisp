;;; -*- Mode:Lisp; Package:XLIB; Base:10; Lowercase:YES; Patch-file:T; Syntax:Common-Lisp -*-

(in-package :cluei)


;;
;; Drawables
;;

;; Allow change in metaclass (structure-class to standard-class)
;;(setf (find-class 'drawable nil) nil)

(defclass drawable ()
  ((id      :type     resource-id
	    :initform 0
	    :accessor drawable-id
	    :initarg  :id)

   (display :type     (or null display)
	    :initform nil
	    :accessor drawable-display
	    :initarg  :display)

   (plist   :type     list
	    :initform nil
	    :accessor drawable-plist
	    :initarg  :plist))		; Extension hook

  (:documentation "The class of CLX drawable objects."))


(defun make-drawable (&rest initargs)
  (apply #'make-instance 'drawable initargs))

(defun drawable-p (object)
  (typep object 'drawable))

;;
;; Windows
;;

;; Allow change in metaclass (structure-class to standard-class)
;;(setf (find-class 'window nil) nil)

(defclass window (drawable)
  ((id      :type     resource-id
	    :initform 0
	    :accessor window-id
	    :initarg  :id)

   (display :type     (or null display)
	    :initform nil
	    :accessor window-display
	    :initarg  :display)

   (plist   :type     list
	    :initform nil
	    :accessor window-plist
	    :initarg  :plist))		; Extension hook

  (:documentation "The class of CLX window objects."))


(defun make-window (&rest initargs)
  (apply #'make-instance 'window initargs))

(defun window-p (object)
  (typep object 'window))


;;
;; Pixmaps
;;

;; Allow change in metaclass (structure-class to standard-class)
;;;(setf (find-class 'pixmap nil) nil)

(defclass pixmap (drawable)
  ((id      :type     resource-id
	    :initform 0
	    :accessor pixmap-id
	    :initarg  :id)

   (display :type     (or null display)
	    :initform nil
	    :accessor pixmap-display
	    :initarg  :display)

   (plist   :type     list
	    :initform nil
	    :accessor pixmap-plist
	    :initarg  :plist))		; Extension hook

  (:documentation "The class of CLX pixmap objects."))


(defun make-pixmap (&rest initargs)
  (apply #'make-instance 'pixmap initargs))

(defun pixmap-p (object)
  (typep object 'pixmap))
