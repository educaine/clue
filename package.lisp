;;; -*- Mode:Lisp; Package:CLUEI; Syntax:COMMON-LISP; Base:10; Lowercase:T -*-

;;;
;;;			 TEXAS INSTRUMENTS INCORPORATED
;;;				  P.O. BOX 149149
;;;			       AUSTIN, TEXAS 78714-9149
;;;
;;; Copyright (C)1987,1988,1989,1990 Texas Instruments Incorporated.
;;;
;;; Permission is granted to any individual or institution to use, copy, modify,
;;; and distribute this software, provided that this complete copyright and
;;; permission notice is maintained, intact, in all copies and supporting
;;; documentation.
;;;
;;; Texas Instruments Incorporated provides this software "as is" without
;;; express or implied warranty.
;;;

;;(in-package :cluei)

;; Export symbols external to CLUEI from CLUE
;; This enables an application to use the CLUE package and get everything.
;; (do-external-symbols (sym (find-package 'cluei))
;;   (export sym 'clue))

(defpackage :mop
  (:use :closer-mop)
  (:export
   :built-in-class
   :class
   :direct-slot-definition
   :effective-slot-definition
   :eql-specializer
   :forward-referenced-class
   :funcallable-standard-class
   :funcallable-standard-object
   :generic-function
   :metaobject
   :method
   :method-combination
   :slot-definition
   :specializer
   :standard-accessor-method
   :standard-class
   :standard-generic-function
   :standard-direct-slot-definition
   :standard-effective-slot-definition
   :standard-method
   :standard-object
   :standard-reader-method
   :standard-slot-definition
   :standard-writer-method
   :defclass
   :defgeneric
   :define-method-combination
   :defmethod
   :classp
   :ensure-finalized
   :ensure-method
   :fix-slot-initargs
   :required-args
   :subclassp
   :accessor-method-slot-definition
   :add-dependent
   :add-direct-method
   :add-direct-subclass
   :class-default-initargs
   :class-direct-default-initargs
   :class-direct-slots
   :class-direct-subclasses
   :class-direct-superclasses
   :class-finalized-p
   :class-precedence-list
   :class-prototype
   :class-slots
   :compute-applicable-methods-using-classes
   :compute-class-precedence-list
   :compute-default-initargs
   :compute-discriminating-function
   :compute-effective-method
   :compute-effective-method-function
   :compute-effective-slot-definition
   :compute-slots
   :direct-slot-definition-class
   :effective-slot-definition-class
   :ensure-class
   :ensure-class-using-class
   :ensure-generic-function
   :ensure-generic-function-using-class
   :eql-specializer-object
   :extract-lambda-list
   :extract-specializer-names
   :finalize-inheritance
   :find-method-combination
   :funcallable-standard-instance-access
   :generic-function-argument-precedence-order
   :generic-function-declarations
   :generic-function-lambda-list
   :generic-function-method-class
   :generic-function-method-combination
   :generic-function-methods
   :generic-function-name
   :intern-eql-specializer
   :make-method-lambda
   :map-dependents
   :method-function
   :method-generic-function
   :method-lambda-list
   :method-specializers
   :reader-method-class
   :remove-dependent
   :remove-direct-method
   :remove-direct-subclass
   :set-funcallable-instance-function
   :slot-boundp-using-class
   :slot-definition-allocation
   :slot-definition-initargs
   :slot-definition-initform
   :slot-definition-initfunction
   :slot-definition-location
   :slot-definition-name
   :slot-definition-readers
   :slot-definition-writers
   :slot-definition-type
   :slot-makunbound-using-class
   :slot-value-using-class
   :specializer-direct-generic-functions
   :specializer-direct-methods
   :standard-instance-access
   :subtypep
   :typep
   :update-dependent
   :validate-superclass
   :writer-method-class
   :warn-on-defmethod-without-generic-function))


(defpackage :cluei
  (:use :common-lisp)
  (:export
   :clue
   display-root ;; Setf'able
   display-root-list
   display-multipress-delay-limit
   display-multipress-verify-p
   *default-host*
   *default-display*
   *default-multipress-delay-limit*
   *default-multipress-verify-p*
   *parent*				; Bound during contact initialization
   open-contact-display
   basic-contact
   contact
   make-contact
   ;; Contact slots:
   display
   parent
   complete-name
   complete-class
   callbacks
   event-translations
   state
   sensitive
   compress-motion
   compress-exposures
   x y width height
   border-width
   background
   depth
   event-mask
   id
   plist
   ;; Contact slot accessors:
   contact-background
   contact-border-width
   contact-callbacks
   contact-compress-exposures
   contact-compress-motion
   contact-depth
   contact-display
   contact-event-mask
   contact-height
   contact-parent
   contact-sensitive
   contact-state
   contact-width
   contact-x
   contact-y
   composite
   children focus shells
   composite-children
   composite-focus
   composite-shells
   destroy
   contact-complete-name
   contact-complete-class
   contact-name
   display-name
   display-class
   ;;	  find-contact
   ancestor-p
   realized-p
   destroyed-p
   mapped-p
   top-level-p
   managed-p
   sensitive-p
   event-mask  ;; Setf'able
   resource
   update-state
   initialize-geometry
   present
   dismiss
   realize
   display
   refresh
   owns-focus-p
   inside-contact-p
   add-callback
   apply-callback
   apply-callback-else
   callback-p
   delete-callback
   root
   contact-root
   contact-screen
   contact-translate
   contact-top-level
   read-character
   unread-character
   listen-character
   append-characters
   clear-characters
   add-child
   delete-child
   previous-sibling
   next-sibling
   change-priority
   manage-priority
   accept-focus-p
   move-focus
   change-layout
   while-changing-layout
   change-geometry
   preferred-size
   move
   resize
   manage-geometry

   spring-loaded
   shadow-width
   contact-constraints
   contact-constraint
   class-constraints

   display-cursor
   contact-image-cursor
   contact-glyph-cursor
   contact-mask
   contact-image-mask
   contact-image-pixmap
   contact-pixmap
   display-mask
   display-pixmap
   using-gcontext




   event
   ;; Event slots
   key display contact character keysym above-sibling
   atom border-width child code colormap configure-p count data
   drawable event-window focus-p format height hint-p installed-p
   keymap kind major minor mode name new-p override-redirect-p
   parent place property requestor root root-x root-y same-screen-p
   selection send-event-p state target time type width window x y
   mode-type
   *remap-events*
   *restrict-events*
   add-mode
   delete-mode
   with-mode
   with-event-mode
   contact-mode
   contact-super-mode
   *contact*
   apply-action
   call-action
   defaction
   describe-action
   eval-action
   ignore-action
   perform-callback
   throw-action
   trace-action
   with-event
   processing-event-p
   add-before-action
   delete-before-action
   process-all-events
   process-next-event
   handle-event
   translate-event
   defevent
   undefevent
   event-actions
   add-event
   delete-event
   describe-event-translations
   add-timer
   delete-timer


   virtual
   virtual-composite


   rectangle glyphs clear


   contact-root-shell
   override-shell
   shell
   shell-mapped
   shell-owner
   shell-unmapped
   sm-client-host
   sm-command
   top-level-session
   top-level-shell
   transient-shell
   with-wm-properties
   with-wm-properties
   wm-base-height
   wm-base-width
   wm-colormap-owners
   wm-delta-height
   wm-delta-width
   wm-gravity
   wm-group
   wm-icon
   wm-icon-mask
   wm-icon-title
   wm-icon-x
   wm-icon-y
   wm-initial-state
   wm-keyboard-input
   wm-max-aspect
   wm-max-height
   wm-max-width
   wm-message
   wm-message-protocol
   wm-message-timestamp
   wm-min-aspect
   wm-min-height
   wm-min-width
   wm-protocols-used
   wm-shell
   wm-title
   wm-user-specified-position-p
   wm-user-specified-size-p

   interactive-stream
   stream-clear-input
   stream-unread-char
   stream-listen
   stream-peek-char
   stream-read-char
   stream-read-line
   set-cursorpos
   stream-clear-output
   stream-move-cursor
   stream-write-char
   clear-line
   clear-eol
   stream-write-string
   text-within-width
   stream-fresh-line
   draw-lozenged-string
   simple-rubout-handler
   with-input-editing
   rubout-handler
   get-rubout-handler-buffer
   force-input
   make-interactive-stream ;; may 12/14/89
   lisp-listener
   listener
   check-function))

(defpackage :clue
  (:use :common-lisp :cluei)
  (:export :window-documentation
	   :change-window-documentation))

(defpackage :clue-examples
  (:use :common-lisp :cluei))


;;;(make-package "CLUEI" :use '(common-lisp xlib))
