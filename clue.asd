(asdf:defsystem :clue
  :depends-on (:clx :closer-mop)
  :components
  ((:file "package")
   ;;      (:module clx-macros
   ;; 	      :pathname ""
   ;; 	      :depends-on (:package)
   ;; 	      :components
   ;; 	      (
   ;; ;;	       (:file "macros")
   ;; ;;	       (:file "bufmac")
   ;; 	       ))
   (:module clx-patches
	    :pathname ""
	    :depends-on (:package)
	    :components
	    (
	     ;;	       (:file "clx-patch")
	     (:file "window-doc")))
   (:module serial
	    :pathname ""
	    :depends-on (:clx-patches)
	    :components
	    (
	     (:file "clos-patch")
	     (:file "event-parse")
	     (:file "defcontact")
	     (:file "intrinsics")
	     (:file "caches")
	     (:file "resource")
	     (:file "gray")
	     (:file "cursor")
	     (:file "events")
	     (:file "virtual")
	     (:file "shells")
	     (:file "stream")
	     (:file "root-gmgmt")
	     ))
   (:module test
	    :depends-on (:clx-patches)
	    :components
	    (
	     (:file "menu")
	     ))))
